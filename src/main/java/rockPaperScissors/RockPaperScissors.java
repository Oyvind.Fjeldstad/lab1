package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    
    public void run() {
        while(true) {
            System.out.println("Let's play round " + roundCounter);
            roundCounter++;
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String playerMove = sc.nextLine();
            
            Random random = new Random();
            int randomNumber = random.nextInt(3);
            String computerMove = rpsChoices.get(randomNumber);

            System.out.print("Human chose " + playerMove + ", computer chose " + computerMove + ". ");

            if (playerMove.equals(computerMove)) {
                System.out.println("It's a tie!");
            } else if (playerMove.equals("rock")) {
                if (computerMove.equals("scissors")) {
                    System.out.println("Human wins!");
                    humanScore++;
                } else {
                    System.out.println("Computer wins!");
                    computerScore++;
                }
            } else if (playerMove.equals("paper")) {
                if (computerMove.equals("rock")) {
                    System.out.println("Human wins!");
                    humanScore++;
                } else {
                    System.out.println("Computer wins!");
                    computerScore++;
                }
            } else if (playerMove.equals("scissors")) {
                if (computerMove.equals("paper")) {
                    System.out.println("Human wins!");
                    humanScore++;
                } else {
                    System.out.println("Computer wins!");
                    computerScore++;
            }}
            
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            System.out.println("Do you wish to continue playing? (y/n)?");
            String playagain = sc.nextLine();

            if (playagain.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }

        }
    }
}
